
public class Q2
{
	public static void main(String[] args) {
		System.out.println(compareVersion("0.1","1.1"));
	}
	static short compareVersion(String version1, String version2){
		String[] version1Arr = version1.split("\\.");
		String[] version2Arr = version2.split("\\.");
		Integer[] version1ArrInt = new Integer[version1Arr.length];
		Integer[] version2ArrInt = new Integer[version2Arr.length];
		for (int i = 0; i < version1Arr.length; i++) {
			Integer num = str2Int(version1Arr[i]);
			if(num == null) return 0;
			version1ArrInt[i] = num;
		}
		for (int i = 0; i < version2Arr.length; i++) {
			Integer num = str2Int(version2Arr[i]);
			if(num == null) return 0;
			version2ArrInt[i] = num;
		}
		if(version2ArrInt.length > version1ArrInt.length){
			for (int i = 0; i < version1ArrInt.length; i++) {
				if(version1ArrInt[i] > version2ArrInt[i]){
					return 1;
				}else if(version1ArrInt[i] < version2ArrInt[i]){
					return -1;
				}
			}
			for (int i = version1ArrInt.length; i < version2ArrInt.length-version1ArrInt.length; i++) {
				if(version2ArrInt[i] > 0){
					return -1;
				}
			}
		}else{
			for (int i = 0; i < version2ArrInt.length; i++) {
				if(version2ArrInt[i] > version1ArrInt[i]){
					return -1;
				}else if(version2ArrInt[i] < version1ArrInt[i]){
					return 1;
				}
			}
			for (int i = version2ArrInt.length; i < version1ArrInt.length-version2ArrInt.length; i++) {
				if(version1ArrInt[i] > 0){
					return 1;
				}
			}
		}
		return 0;
	}
	static Integer str2Int(String string){
		try {
			return Integer.parseInt(string);
		} catch (NumberFormatException e) {
			return null;
		}
	}
}