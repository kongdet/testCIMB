
public class Q1
{
	public static void main(String[] args) {
		System.out.println(calProfitMax(new Short[]{7,1,5,3,6,4}));
	}

	static short calProfitMax(Short[] prices){
		short profit = 0;
		Short minPrice = prices[0];
		for (int i = 0; i < prices.length-1; i++) {
			if(minPrice > prices[i]){
				minPrice = prices[i];
			}
			Short maxPrice = 0;
			for (int j = i+1; j < prices.length; j++) {
				if(maxPrice < prices[j]){
					maxPrice = prices[j];
					if(maxPrice - minPrice > profit){
						profit = (short) (maxPrice - minPrice);
					}
				}
			}
		}
		return profit;
	}
}