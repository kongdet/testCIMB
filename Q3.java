public class Q3
{
	public static void main(String[] args) {
		System.out.println(findNumDiffPattern(7));
	}
	static int findNumDiffPattern(int n){
		if(n > 2){
			return findNumDiffPattern(n - 2) + findNumDiffPattern(n - 1);
		}
		if(n==2){
			return 2;
		}
		if(n==1){
			return 1;
		}
		return 0;
	}
}